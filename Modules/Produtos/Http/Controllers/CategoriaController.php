<?php

namespace Modules\Produtos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Produtos\Repositories\Contracts\CategoriaRepositoryInterface;

class CategoriaController extends BaseController
{

    private $categoriaRepo;
    public function __construct(CategoriaRepositoryInterface $categoriaRepository)
    {
        $this->categoriaRepo = $categoriaRepository;
    }

    /**
     * Display a listing of the resource.
     * @return Response
     */
    public function index()
    {
        $categorias = $this->categoriaRepo->all();
        
        #return view('produtos::index', compact('categorias'));
        return $this->sendResponse($categorias, 'Categorias listadas com sucesso');
    }

    /**
     * Show the form for creating a new resource.
     * @return Response
     */
    public function create()
    {
        return view('produtos::create');
    }

    /**
     * Store a newly created resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function store(Request $request)
    {
    }

    /**
     * Show the specified resource.
     * @return Response
     */
    public function show()
    {
        return view('produtos::show');
    }

    /**
     * Show the form for editing the specified resource.
     * @return Response
     */
    public function edit()
    {
        return view('produtos::edit');
    }

    /**
     * Update the specified resource in storage.
     * @param  Request $request
     * @return Response
     */
    public function update(Request $request)
    {
    }

    /**
     * Remove the specified resource from storage.
     * @return Response
     */
    public function destroy()
    {
    }
}