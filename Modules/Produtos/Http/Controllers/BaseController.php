<?php

namespace Modules\Produtos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;

class BaseController extends Controller
{
    /**
     * success response method.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendResponse($result, $message)
    {

        if(is_null($result)){
            $response = [
                'sucesso' => true,
                'mensagem' => $message,
            ];
        }else{
            $response = [
                'sucesso' => true,
                'data'    => $result,
                'mensagem' => $message,
            ];
        }

        return response()->json($response, 200);
    }

    /**
     * return error response.
     *
     * @return \Illuminate\Http\Response
     */

    public function sendError($error, $errorMessages = [], $code = 404)
    {
        $response = [
            'sucesso' => false,
            'mensagem' => $error,
        ];

        if(!empty($errorMessages)){
            $response['data'] = $errorMessages;
        }

        return response()->json($response, $code);
    }

}
