<?php

namespace Modules\Produtos\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Routing\Controller;
use Modules\Produtos\Http\Controllers\BaseController as BaseController;

class CheckAuthController extends BaseController
{
    public function login(Request $request)
    {
        $credentials = [
            'email' => $request->email,
            'password' => $request->password
        ];

        if (auth()->attempt($credentials)) {
            $token = auth()->user()->createToken('MyApp')->accessToken;
            $success['token'] = $token;
            return $this->sendResponse($success, 'Usuário logado com sucesso.');
        } else {
            return $this->sendError('Usuario não autorizado', null);
        }
    }
}
