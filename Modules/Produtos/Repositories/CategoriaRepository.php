<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 15/11/2018
 * Time: 09:24
 */

namespace Modules\Produtos\Repositories;
use Modules\Produtos\Entities\Categoria;
use Modules\Produtos\Repositories\Contracts\CategoriaRepositoryInterface;

class CategoriaRepository implements CategoriaRepositoryInterface
{

    /**
     * CategoriaRepository constructor.
     */
    public function __construct(Categoria $categoria)
    {
        $this->categoria = $categoria;
    }

    public function all()
    {
        return $this->categoria->all();
    }
}