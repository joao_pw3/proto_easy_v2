<?php
/**
 * Created by PhpStorm.
 * User: Alexandre
 * Date: 15/11/2018
 * Time: 09:35
 */

namespace Modules\Produtos\Repositories\Contracts;


interface CategoriaRepositoryInterface
{
    public function all();
}